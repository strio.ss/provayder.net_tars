function scrollHeader() {
    if ($(window).width() < 750) {
        var countTop = 55;
    } else {
        var countTop = 227;
    }

    if ($(this).scrollTop() > 55) {
        $('.header-fixed').addClass('header-fixed_show');
    } else {
        $('.header-fixed').removeClass('header-fixed_show');
    }
}

$(window).scroll(scrollHeader);

$(function () {

    // Sort Street
    $('.select-page-b__street .select-page-b__sort-links').on('click', function() {
        var label = $(this).text()

        $('.select-page-b__list').addClass('select-page-b__list_single');

        $(this).closest('.select-page-b__sort-list').find('.select-page-b__sort-links').removeClass('select-page-b__sort-links_current');
        $(this).addClass('select-page-b__sort-links_current');

        $('.select-page-b__column').hide();
        $('.select-page-b__column[data-select="'+ label +'"]').show();

        if (label == 'Все') {
            $('.select-page-b__list').removeClass('select-page-b__list_single');
            $('.select-page-b__column').show();
        }

        return false;
    });

    $('.select-page-b__streets a').on('click', function() {
        $('.select-page-b__street').hide();
        $('.select-page-b__house').show();

        return false;
    });

    $('.select-page-b__house .select-page-b__sort-links').on('click', function() {
        $(this).closest('.select-page-b__sort-list').find('.select-page-b__sort-links').removeClass('select-page-b__sort-links_current');
        $(this).addClass('select-page-b__sort-links_current');

        return false;
    });

    // Sort Street End

    $(window).scroll(scrollHeader);

    $(document).on('click', function(e){
        if ($(e.target).closest('.header-mobile-b__select').length) return;
        $('.header-mobile-b__select-drop').stop(true,true).slideUp('fast');
        $('.header-mobile-b__select-main').removeClass('header-mobile-b__select_active');
        e.stopPropagation();
    });
    $('.js--mobile-select').on('click', function() {
        $('.header-mobile-b__select-main').toggleClass('header-mobile-b__select-main_active');
        $('.header-mobile-b__select-drop').stop(true,true).slideToggle('fast');

       return false;
    });

    $('.js--city-confirm').on('click', function() {
        $('.header-b__city-drop').hide();
       return false;
    });

    $('.js--toggle-contactsInfo').on('click', function() {
        $(this).closest('.contacts-block-b__more').hide();
        $(this).closest('.contacts-block-b__column').find('.contacts-block-b__field_hidden').show();

       return false;
    });

    $('.js--toggle-contactsText').on('click', function() {
        $(this).closest('.contacts-block-b__more').hide();
        $(this).closest('.contacts-block-b__column').find('.contacts-block-b__text').show();

       return false;
    });

    $('.js--toggle-compare').on('click', function() {
        $('.header-fixed').toggleClass('header-fixed_show');
        $('.header-compare').toggleClass('header-compare_show');

        var el  = $('span', this);
        el.text(el.text() == '+' ? '-' : '+');

        $(this).toggleClass('tariffs-b__button-compare_active');

       return false;
    });

    $('.js--toggle-text').on('click', function() {
        $(this).closest('.contacts-block-b__more').hide();
        $('.about-block-b__text').addClass('about-block-b__text_show');

       return false;
    });

    $('[data-select]').on('click', function() {
        var data = $(this).attr('data-select');

        if ((data == 'reviews') || (data == 'sales')|| (data == 'rating')) {
            $('.select-b__block[data-select="'+ data +'"]').addClass('select-b__block_active');
            $('[data-show="'+ data +'"]').show();
        }

        if ($(window).width() < 750) {
            var selectTop = 80;
        } else {
            var selectTop = 120;
        }

        $('html,body').animate({
            scrollTop: $('[data-show="'+ data +'"]').offset().top - selectTop
        });

        return false;
    });

    $('.js--tariffs-more').on('click', function() {
        $(this).closest('.tariffs-b').find('.sales-b__more').hide();
        $('.tariffs-b__table-row').addClass('tariffs-b__table-row_show');
        return false;
    });

    $('.reviews-b__rate-control').on('click', function() {
        $(this).closest('.reviews-b__item').find('.reviews-b__rate-control').removeClass('reviews-b__rate-control_active');
        $(this).addClass('reviews-b__rate-control_active');
        return false;
    });

    $('.live-b__rate-control').on('click', function() {
        $(this).closest('.live-b__item').find('.live-b__rate-control').removeClass('live-b__rate-control_active');
        $(this).addClass('live-b__rate-control_active');
        return false;
    });

    $('.reviews-b__actions .sales-b__go').on('click', function() {
        $(this).hide();
        $(this).closest('.reviews-b__item').find('.reviews-b__text').addClass('reviews-b__text_show');
        return false;
    });

    var tabHActiveWidth = $('.header-b__tabs-links_active').width(),
        tabHActivePos = $('.header-b__tabs-links_active').position();

    $('.header-b .header-b__tabs-baloon').css({
        left: tabHActivePos.left,
        width: tabHActiveWidth
    });

    $('.modal-b .header-b__tabs-baloon').css({
        left: tabHActivePos.left,
        width: tabHActiveWidth
    });

    $('.header-b__tabs-links').on('click', function() {
       $('.header-b__tabs-links').removeClass('header-b__tabs-links_active');
       $(this).addClass('header-b__tabs-links_active');

        var tabHActiveWidth = $('.header-b__tabs-links_active').width(),
            tabHActivePos = $('.header-b__tabs-links_active').position();

        $('.header-b .header-b__tabs-baloon').css({
            left: tabHActivePos.left,
            width: tabHActiveWidth
        });

        $('.modal-b .header-b__tabs-baloon').css({
            left: tabHActivePos.left,
            width: tabHActiveWidth
        });

       return false;
    });


    $('.slider-b').each(function() {
        var tabActiveWidth = $('.slider-b__tabs-links_active').width(),
            tabActivePos = $('.slider-b__tabs-links_active').position();

        if ($(window).width() < 750) {
            tabActiveWidth = tabActiveWidth + 44
        } else {
            tabActiveWidth = tabActiveWidth + 74
        }

        $('.slider-b__tabs-baloon').css({
            left: tabActivePos.left,
            width: tabActiveWidth
        });
    });

    $('.slider-b__tabs-links').on('click', function() {
       $('.slider-b__tabs-links').removeClass('slider-b__tabs-links_active');
       $(this).addClass('slider-b__tabs-links_active');

        var tabActiveWidth = $('.slider-b__tabs-links_active').width(),
            tabActivePos = $('.slider-b__tabs-links_active').position();

        if ($(window).width() < 750) {
            tabActiveWidth = tabActiveWidth + 44
        } else {
            tabActiveWidth = tabActiveWidth + 74
        }

        $('.slider-b__tabs-baloon').css({
            left: tabActivePos.left,
            width: tabActiveWidth
        });

        var index = $(this).closest('.slider-b__tabs-item').index();

        $('.tariffs-b__content').removeClass('tariffs-b__content_show');
        $('.tariffs-b__content').eq(index).addClass('tariffs-b__content_show');

        return false;
    });

    $('.js--toggle-tariff').on('click', function() {
       $(this).toggleClass('tariffs-b__more_active');
       $(this).closest('.tariffs-b__table-row').find('.tariffs-b__toggle').stop(true,true).slideToggle('fast');
       $(this).closest('.tariffs-b__table-row').toggleClass('tariffs-b__table-row_active');

        if ($(window).width() < 750) {
            $('.header-mobile-fixed').toggle();
            $('.header-tariff').toggleClass('header-tariff_show');
        }
    });

    $('.filter-b__select-main').on('click', function() {
       $(this).closest('.filter-b__select').toggleClass('filter-b__select_show');
       $(this).closest('.filter-b__select').find('.filter-b__select-drop').stop(true,true).slideToggle('fast');
    });



	
	

    $(document).on('click', function(e){
        if ($(e.target).closest('.filter-b__select').length) return;
        $('.filter-b__select').removeClass('filter-b__select_show');
        $('.filter-b__select-drop').stop(true,true).slideUp('fast');
        e.stopPropagation();
    });

    $('.qa-b__head').on('click', function() {
       $(this).toggleClass('qa-b__head_active');
       $(this).closest('.qa-b__item').find('.qa-b__text').stop(true,true).slideToggle('fast');
    });

    $('.js--mask-phone').inputmask({
        mask: '+7 (999) 999-99-99',
        showMaskOnHover: false,
        clearIncomplete: true
    });

    var rev = $('.js--slider-main');
    rev.on('init', function(event, slick, currentSlide) {
        var
            cur = $(slick.$slides[slick.currentSlide]),
            next = cur.next(),
            next2 = cur.next().next(),
            prev = cur.prev(),
            prev2 = cur.prev().prev();
        prev.addClass('slick-sprev');
        next.addClass('slick-snext');
        prev2.addClass('slick-sprev2');
        next2.addClass('slick-snext2');
        cur.removeClass('slick-snext').removeClass('slick-sprev').removeClass('slick-snext2').removeClass('slick-sprev2');
        slick.$prev = prev;
        slick.$next = next;
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        console.log('beforeChange');
        var
            cur = $(slick.$slides[nextSlide]);
        console.log(slick.$prev, slick.$next);
        slick.$prev.removeClass('slick-sprev');
        slick.$next.removeClass('slick-snext');
        slick.$prev.prev().removeClass('slick-sprev2');
        slick.$next.next().removeClass('slick-snext2');
        next = cur.next(),
            prev = cur.prev();
        //prev2.prev().prev();
        //next2.next().next();
        prev.addClass('slick-sprev');
        next.addClass('slick-snext');
        prev.prev().addClass('slick-sprev2');
        next.next().addClass('slick-snext2');
        slick.$prev = prev;
        slick.$next = next;
        cur.removeClass('slick-next').removeClass('slick-sprev').removeClass('slick-next2').removeClass('slick-sprev2');
    });

    rev.slick({
        speed: 1000,
        arrows: true,
        dots: true,
        focusOnSelect: true,
        prevArrow: '<button class="slick-arrow slick-prev"></button>',
        nextArrow: '<button class="slick-arrow slick-next"></button>',
        infinite: true,
        centerMode: true,
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerPadding: '0',
        swipe: true,
        customPaging: function(slider, i) {
            return '';
        },
    });

    $('.js--slider-anchor').on('click', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 120
        }, 300);
    });

    $('.js--compare-list').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false,
    });

    $('.js--slider-welcome').slick({
        slidesToShow: 1,
        dots:true
    });

    $('.main-block-b__find-help').on('mouseenter', function() {
        $(this).closest('.main-block-b__find').find('.main-block-b__find-toggle').stop(true,true).fadeIn('fast');
    });

    $('.main-block-b__find-help').on('mouseleave', function() {
        $(this).closest('.main-block-b__find').find('.main-block-b__find-toggle').stop(true,true).fadeOut('fast');
    });

    $('.js--toggle-help').on('mouseenter', function() {
       $(this).closest('.rate-b__help').find('.main-block-b__find-toggle').stop(true,true).fadeIn('fast');
    });

    $('.js--toggle-help').on('mouseleave', function() {
        $(this).closest('.rate-b__help').find('.main-block-b__find-toggle').stop(true,true).fadeOut('fast');
    });

});



$(function () {

    (function () {
        var body = document.getElementsByTagName('body')[0];
        var bodyScrollTop = null;
        var locked = false;
        var scrollBarWidth = '0px';

        const setScrollbarWidthVar = () => {
            const getScrollBarWidth = () => {
                const d = document.createElement('div');
                d.className = 'scrollbar-width-checker';
                d.style.overflowY = 'scroll';
                d.style.width = '50px';
                d.style.height = '50px';
    
                document.body.append(d);
                const result = d.offsetWidth - d.clientWidth;
                d.remove();
                return result;
            }
            scrollBarWidth = getScrollBarWidth() + 'px'
            document.documentElement.style.setProperty('--scrollbar-width', scrollBarWidth);
        }        

        setScrollbarWidthVar();
        
        // Определяем функцию, запрещающую скроллинг
        function preventScroll(e) {
            e.preventDefault();
            return false
        }

        window.lockScroll = function () {

            bodyScrollTop = (typeof window.pageYOffset !== 'undefined') ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
            body.classList.add('scroll-locked');
            body.style.paddingRight = scrollBarWidth;
            body.style.top = -bodyScrollTop + 'px';
            locked = true;

            $(document).on('scroll', preventScroll);
        };

        window.unlockScroll = function () {

            body.classList.remove('scroll-locked');
            body.style.paddingRight = '';
            body.style.top = null;
            window.scrollTo(0, bodyScrollTop);

            $(document).off('scroll', preventScroll);
        };
    }());

    $(document).on('click', '[data-show-modal]', function (e) {
        e.preventDefault();
        var $modal = $('#' + $(this).data('show-modal'));
        $modal.fadeIn('fast');

        var tabHActiveWidth = $('.modal-b .header-b__tabs-links_active').width(),
            tabHActivePos = $('.modal-b .header-b__tabs-links_active').position();

        $('.modal-b .header-b__tabs-baloon').css({
            left: tabHActivePos.left,
            width: tabHActiveWidth
        });

        lockScroll();
    });

    $('.js--close-modal').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.modal-b').fadeOut('fast');
        unlockScroll();
    });

    $(document).on('keyup', function (e) {
        if (e.key === "Escape") {
            e.preventDefault();
            $('.modal-b').fadeOut('fast');
            unlockScroll();
        }
    });

    $('.modal-b').on('click', function (e) {
        console.log('click on modal-b',e.target);
        if ($(e.target).closest('.modal-b__block').length) return;
        //if ($(e.target).closest('.popup-wrap'.length)) return;

        if (e.target.classList.contains('modal-b') || e.target.classList.contains('popup-wrap')){

            e.preventDefault();
            $(this).closest('.modal-b').fadeOut('fast');
            unlockScroll();
            e.stopPropagation();
        }
    });

    $('.modal-b__list_company .modal-b__links').on('click', function() {
        var companyLabel = $(this).text();
        $('.js--selected-company').text(companyLabel);

        $(this).closest('.modal-b').fadeOut('fast');
        unlockScroll();
    });

});





/*
$(function(){
	$('.banner-connect-b__field_street input').on('blur', function(){
		this.parentNode.classList.remove('banner-connect-b__field__show');
	});
	$('.banner-connect-b__field_house input').on('blur', function(){
		this.parentNode.classList.remove('banner-connect-b__field__show');
	});
	$('.banner-connect-b__field_street').on('click', function(){
		let els_active = this.parentNode.querySelectorAll('.banner-connect-b__field__show');
		for(let el of els_active){
			if (el !== this)
				el.classList.remove('banner-connect-b__field__show');
		}
		this.classList.toggle('banner-connect-b__field__show');
	});
	$('.banner-connect-b__field_house').on('click', function(){
		let els_active = this.parentNode.querySelectorAll('.banner-connect-b__field__show');
		for(let el of els_active){
			if (el !== this)
				el.classList.remove('banner-connect-b__field__show');
		}
		this.classList.toggle('banner-connect-b__field__show');
	});
	
	
	$('.choose-items').on('click', function(e){
		$(this).toggleClass('choose-items__active');
	});
	
	$('.choose__body').on('click', function(e){
		if ($(this).hasClass('choose__body__checkbox') ){
			if ($(e.target).hasClass('choose__body__item') || e.target.tagName == 'LABEL' ){
				let $el_input = $(e.target).find('input');
				if ($el_input.attr('checked') == 'checked'){
					$el_input.attr('checked', '');
				} else {
					$el_input.attr('checked', 'checked');
				}
			}
			e.stopPropagation();
		} else {
			
		}
	});
	
	
	$('.calculator__choose input').on('blur', function(){
		$(this).parent().removeClass('choose-items__active');
	});
	
});
*/

document.addEventListener('DOMContentLoaded', ()=>{
    let els_tariffs_filter = document.querySelectorAll('.all-providers-tariffs__filter > ul');
    els_tariffs_filter.forEach( el => {
        el.addEventListener('click', e => {
            if (e.target.tagName === "LI"){
                let el_active = el.querySelector('.active');
                if (el_active){
                    el_active.classList.remove('active');
                }
                e.target.classList.add('active');
            }
        })
    });

    let els_reviews_filter = document.querySelectorAll('.reviews-providers__filter > ul');
    els_reviews_filter.forEach( el => {
        el.addEventListener('click', e => {
            if (e.target.tagName === "LI"){
                let el_active = el.querySelector('.active');
                if (el_active){
                    el_active.classList.remove('active');
                }
                e.target.classList.add('active');
            }
        })
    });    

    let els_choose_items = document.querySelectorAll('.choose-items');
    els_choose_items.forEach( el => {
        var timer_id__blur = null;

        el.addEventListener('click', e => {
            const open_dropdown = function (el){
                el.classList.add('choose-items--drop');
                setTimeout(()=>{
                    el.classList.add('choose-items--open');
                }, 25);
            }
            const close_dropdown = function (el){
                el.classList.remove('choose-items--open');
                setTimeout(()=>{
                    el.classList.remove('choose-items--drop');
                }, 300);
            }
            const toggle_dropdown = function (el){
                if (el.classList.contains('choose-items--open') ){
                    close_dropdown(el)
                } else {
                    open_dropdown(el)
                }
            }
            // удалить значение по умолчанию (если не нужна такая логика - убрать функцию)
            const remove_default = function (el_dropdown){
                if (el_dropdown.hasAttribute('data-default-value')){
                    el_dropdown.removeAttribute('data-default-value');
                    // задержка, чтобы выпад меню успело закрыться (не обязательно)
                    setTimeout(()=>{
                        let el_ul = el_dropdown.querySelector('.choose__body');
                        if (el_ul){
                            el_ul.removeChild(el_ul.children[0]);
                        }
                    }, 300);
                }
            }
            // добавить значение по умолчанию (если не нужна такая логика - убрать функцию)
            const add_default = function (el_dropdown, default_value){
                if (el_dropdown.hasAttribute('data-reset-value')){
                    if (!el_dropdown.hasAttribute('data-default-value')){
                        el_dropdown.setAttribute('data-default-value', default_value);

                        let el_ul = el_dropdown.querySelector('.choose__body');
                        if (el_ul){
                            let el_li = document.createElement('li');
                            el_li.classList.add('reset-default');
                            el_li.innerText = el_dropdown.getAttribute('data-reset-value')

                            // задержка, чтобы выпад меню успело закрыться (не обязательно)
                            setTimeout(()=>{
                                el_ul.prepend(el_li);
                            }, 300);
                        }
                    }
                }
            }

            if (e.target.classList.contains('choose-items')){
                toggle_dropdown(e.target)
            } else 
            if (e.target.tagName === 'INPUT'){
                toggle_dropdown(e.target.parentNode)
            } else{
                // клик по LI, или по его детям
                let el_li = e.target;
                if ( (e.target.tagName === 'H4') || (e.target.tagName === 'SPAN') ){
                    // клик по детям LI
                    el_li = el_li.parentNode;
                }

                let el_dropdown = el_li.parentNode.parentNode;
                let el_input = el_dropdown.querySelector('input');

                if (el_dropdown.classList.contains('choose-items--checkboxes')){
                    // choose-items является choose-items--checkboxes - щелкаем боксами
                    if (timer_id__blur) clearTimeout(timer_id__blur);

                    if (el_dropdown.classList.contains('choose-items--radio')){
                        let el_checked = el_dropdown.querySelector('.checked');
                        if ( (el_checked) && (el_checked !== el_li) ){
                            el_checked.classList.remove('checked');
                        }
                    }

                    // возвращаем фокус на элемент ввода, для дальнейшего отслеживания blur
                    if (el_input) el_input.focus();

                    el_li.classList.toggle('checked');
                } else {
                    // choose-items не choose-items--checkboxes
                    if (el_input){
                        if (el_li.classList.contains('reset-default')){
                            el_input.value = el_dropdown.getAttribute('data-default-value');
                            remove_default(el_dropdown);
                        } else {
                            add_default(el_dropdown, el_input.value);
                            let el_h4 = el_li.querySelector('h4');
                            if (el_h4){
                                // копируем только улицу, без города
                                el_input.value = el_h4.innerText;
                            } else {
                                el_input.value = el_li.innerText;
                            }
                        }
                    }
                    toggle_dropdown(el_dropdown)
                }
            }
        });

        // обрабатываем потерю фокуса
        let el_input = el.querySelector('input');
        if (el_input){
            el_input.addEventListener('blur', e => {
                console.log('blur', el_input);
                timer_id__blur = setTimeout(()=>{
                    let el_dropdown = el_input.parentNode;
                    if (el_dropdown.classList.contains('choose-items--open') ){
                        el_dropdown.classList.remove('choose-items--open');
                        setTimeout(()=>{
                            el_dropdown.classList.remove('choose-items--drop')
                        }, 300);
                    }
                }, 100)
            });

            if (el_input.hasAttribute('readonly')){
                el_input.addEventListener('select', e => {
                    el_input.selectionStart = el_input.selectionEnd;
                    e.preventDefault();
                })
            }
        }
    });
})