document.addEventListener("DOMContentLoaded", () => {
  let els_search_filter = document.querySelectorAll('.search-quick-filter');
  els_search_filter.forEach( el => {
    el.addEventListener('click', (e) => {
      if (e.target.tagName === 'LI'){
        e.target.classList.toggle('active');
      }
    })
  });

  let els_search_quick_swiper_items = document.querySelectorAll('.search-quick-swiper__item');
  var timer_popup_search = null;

  els_search_quick_swiper_items.forEach( el => {
    el.addEventListener('click', (e) => {
      if (e.target.tagName === 'A'){
        e.preventDefault();
        if ( el.hasAttribute('data-tarif-info') ){
          if (timer_popup_search) clearTimeout(timer_popup_search);

          let data_info = el.dataset.tarifInfo;

          try{
            data_info = JSON.parse(data_info);
          } catch (err){
            console.error('Error while parsing json at tariff object info', err);
            return;
          }

          let el_popup = document.querySelector('.popup-search-quick');
          if (el_popup){
            let el_popup_text = el_popup.querySelector('.popup-search-quick__left-text');
            text = '';
            for (let i=0; i<data_info.texts_left.length; i++){
              text += '<p>' + data_info.texts_left[i] + '</p>';
            }
            el_popup_text.innerHTML = text;

            let el_popup_text_right = el_popup.querySelector('.popup-search-quick__right > p');
            el_popup_text_right.innerHTML = data_info.text_right;

            el_popup.classList.add('active');
            timer_popup_search = setTimeout( () => {
              el_popup.classList.remove('active');
            }, 5000);
          }
        }
      }
    })
  });

})


$('.search-quick-filter').slick({
  infinite: false,
  variableWidth:true,
  slidesToShow: 8,
  slidesToScroll: 1,
  arrows:false,
  centerMode: false, 
  responsive: [
    {
      breakpoint: 1200,

      settings: {
        slidesToShow: 7,
      }
    },
    {
      breakpoint: 1100,

      settings: {
        slidesToShow: 6,
        slidesToScroll: 2,
      }
    },   
    {
      breakpoint: 1000,

      settings: {
        slidesToShow: 5,
        slidesToScroll: 2,
      }
    },     
    {
      breakpoint: 900,

      settings: {
        slidesToShow: 4,
        slidesToScroll: 2,
      }
    }, 
    {
      breakpoint: 660,

      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
      }
    }, 
    {
      breakpoint: 450,

      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },            
  ]  
});




$('.search-quick-swiper').slick({
  infinite: true,
  variableWidth:false,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows:true,
  centerMode: false, 
  responsive: [
    {
      breakpoint: 1150,

      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 1050,

      settings: {
        slidesToShow: 2,
      }
    },    
    {
      breakpoint: 690,

      settings: {
        slidesToShow: 1,
      }
    },           
  ]  
});


