$('.slider__tariffs-of-month__wrapper').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1100,

      settings: {
        slidesToShow: 2,
        centerMode: false,        

      }
    },
    {
      breakpoint: 720,

      settings: {
        slidesToShow: 1,
        centerMode: false,        

      }
    }, 
    {
      breakpoint: 370,

      settings: {
        slidesToShow: 1,
        centerMode: true,        
        centerPadding: '0px',
      }
    },        
  ]  
});