head: {
    defaults: {
        title: 'default title',
        useSocialMetaTags: true
    },
    index: {
        title: 'Provider main',
        useSocialMetaTags: true
    },
	
	order: {
        title: 'Order',
        useSocialMetaTags: true
    }	,
	compare: {
        title: 'Compare',
        useSocialMetaTags: true
    },	
	street: {
        title: 'Street',
        useSocialMetaTags: true
    },	
	
	providerTariffs: {
        title: 'Provider tariffs',
        useSocialMetaTags: true
    },
	providerHouse: {
        title: 'Provider house',
        useSocialMetaTags: true
    },	
	providers: {
        title: 'Providers',
        useSocialMetaTags: true
    }	
	
}